<?php
$arrJumlah=array("Buku"=>13,"Penggaris"=>12,"Pulpen"=>11,"Penghapus"=>10);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrJumlah);
echo "<pre>";

sort($arrJumlah);
reset($arrJumlah);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrJumlah);
echo"</pre>";

rsort($arrJumlah);
reset($arrJumlah);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrJumlah);
echo"<pre>";
?>