<?php
$arrJumlah=array("Buku"=>13,"Penggaris"=>12,"Pulpen"=>11,"Penghapus"=>10);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrJumlah);
echo "<pre>";

asort($arrJumlah);
reset($arrJumlah);
echo "<b>Array setelah diurutkan dengan asort()</b>";
echo "<pre>";
print_r($arrJumlah);
echo"</pre>";

arsort($arrJumlah);
reset($arrJumlah);
echo "<b>Array setelah diurutkan dengan arsort()</b>";
echo "<pre>";
print_r($arrJumlah);
echo"<pre>";
?>